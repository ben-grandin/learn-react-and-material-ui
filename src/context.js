import React, { createContext } from "react";


export const ContextExercises = createContext({});
export const { Provider, Consumer } = ContextExercises;

export const withContext = Component =>
	props => (
		<Consumer>
			{value => {
				return <Component {...value} {...props} />;
			}}
		</Consumer>
	);