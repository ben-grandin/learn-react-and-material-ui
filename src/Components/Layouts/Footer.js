import { AppBar, Tab, Tabs, withWidth } from "@material-ui/core";
import React from "react";
import { withContext } from "../../context";


class Footer extends React.Component {

	getMuscles = () => {
		return ["", ...this.props.muscles];
	};

	muscles = this.getMuscles();

	getIndex = () => {
		return this.muscles.indexOf(this.props.category);
	};

	onIndexSelect = (e, index) => {
		const { onCategorySelect } = this.props;
		onCategorySelect(this.muscles[index]);
	};

	render() {
		let { width } = this.props;
		return <AppBar position="static">
			<Tabs

				value={this.getIndex()}
				onChange={this.onIndexSelect}
				indicatorColor="secondary"
				textColor="secondary"
				variant={width === "xs" ? "scrollable" : "standard"}
				centered={width !== "xs"}

			>
				{this.muscles.map(group =>
					<Tab key={group} label={group || "All"}/>,
				)}
			</Tabs>
		</AppBar>;

	}
}

export default withContext(withWidth()(Footer));
