import { AppBar, Toolbar, Typography, withStyles } from "@material-ui/core";
import React from "react";

import { CreateDialog } from "../Exercises";


const styles = {
	flex: {
		flex: 1,
	},
};

const Header = ({ classes }) =>
	<AppBar position={"static"}>
		<Toolbar>
			<Typography variant={"h5"}
			            color={"inherit"}
			            className={classes.flex}
			>
				Exercise Database
			</Typography>
			<CreateDialog/>
		</Toolbar>
	</AppBar>;

export default withStyles(styles)(Header);
