import { Grid, Paper, withStyles } from "@material-ui/core";
/** There is three way to import rounded icons
 *
 * import { CreateDialog, DeleteRounded } from "@material-ui/icons";
 */
import React from "react";
import { Catalog, Preview } from "./";


const styles = theme => ({
	"@global": {
		"html, body, #root": {
			height: "100%",
		},
	},
	paper: {
		padding: theme.spacing(2),
		overflowY: "auto",
		[theme.breakpoints.up("sm")]: {
			height: "calc(100% -10px)",
		},
		[theme.breakpoints.up("xs")]: {
			height: "100%",
		},
	},
	container: {
		[theme.breakpoints.up("sm")]: {
			height: "calc(100% - 64px - 48px )",
		},
	},
	item: {
		height: "100%",
		padding: "3px",

		[theme.breakpoints.down("xs")]: {
			width: "100%",
			height: "50%",

		},
	},
});

const Exercise = ({ classes },
) => {
	return <Grid container className={classes.container}>
		<Grid item className={classes.item} xs={12} sm={6}>
			<Paper className={classes.paper}>
				<Catalog/>
			</Paper>

		</Grid>
		<Grid item className={classes.item} xs={12} sm={6}>
			<Paper className={classes.paper}>

				<Preview/>
			</Paper>

		</Grid>
	</Grid>;

};

export default withStyles(styles)(Exercise);