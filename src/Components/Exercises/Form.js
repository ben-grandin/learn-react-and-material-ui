import {
	Button,
	FormControl,
	InputLabel,
	MenuItem,
	Select,
	TextField,
} from "@material-ui/core";
import React, { Component } from "react";


class Form extends Component {
	// ToDo: Deprecated : https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs
	state = this.getInitState();

	getInitState() {
		const { exercise } = this.props;
		return exercise ? exercise : {
			title: "",
			description: "",
			muscles: "",
		};

	}

	onChange = ({ target: { value, name } }) => {
		this.setState({
			[name]: value,
		});
	};

	onSubmit = () => {
		// TODO: validate
		const { onSubmit } = this.props;

		onSubmit({
			id: this.state.title.toLocaleLowerCase().replace(" ", "-"),
			...this.state,
		});

		this.setState(this.getInitState());

	};

	render() {
		const { title, description, muscles } = this.state,
			{ exercise, muscles: categories } = this.props;

		return <form>
			<TextField
				label="Title"
				value={title}
				margin="normal"
				name={"title"}
				onChange={this.onChange}
				fullWidth
			/>


			<FormControl fullWidth margin="normal">
				<InputLabel htmlFor="muscles">
					Muscles
				</InputLabel>
				<Select
					value={muscles}
					name={"muscles"}
					onChange={this.onChange}
				>
					{categories.map(category =>
						<MenuItem key={category} value={category}>
							{category}
						</MenuItem>,
					)}
				</Select>
			</FormControl>

			<TextField margin="normal"
			           multiline
			           rows="4"
			           label="Description"
			           value={description}
			           name={"description"}
			           onChange={this.onChange}
			           fullWidth
			/>

			<Button
				variant="contained"
				color="primary"
				onClick={this.onSubmit}
				disabled={ !title || !muscles}
			>
				{exercise ? "Edit" : "Create"}
			</Button>
		</form>;

	}
}

export default Form;