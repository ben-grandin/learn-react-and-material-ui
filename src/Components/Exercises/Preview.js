import { Typography } from "@material-ui/core";
import React from "react";
import { withContext } from "../../context";
import Form from "./Form";


const Preview = ({
	onExerciseEdit,
	editMode,
	muscles,
	exercise,
	exercise: {
		id,
		title,
		description,
	},
}) => {

	return (
		<>
			<Typography color={"secondary"}
			            variant={"h3"}
			            gutterBottom
			>
				{title || "Welcome ! "}
			</Typography>

			{editMode
				? <Form
					key={id}
					exercise={exercise}
					muscles={muscles}
					onSubmit={onExerciseEdit}/>
				: <Typography
					variant={"subtitle1"}
				>
					{description ||
					"Please select an exercise from the list on the left."}
				</Typography>

			}</>
	);

};
export default withContext(Preview)

