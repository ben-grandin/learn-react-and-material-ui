import {
	Dialog,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Fab,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import React, { Component } from "react";
import { ContextExercises } from "../../context";

import Form from "./Form";


const DEFAULT_STATE = {
	open: false,
	exercise: {
		title: "",
		description: "",
		muscles: "",
	},
};

class CreateDialog extends Component {
	static contextType = ContextExercises;
	state = { ...DEFAULT_STATE };

	handleToggle = () => {
		this.setState({
			open: !this.state.open,
		});
	};

	onSubmit = exercise => {
		this.handleToggle();

		this.context.onExerciseCreate(exercise);
	};

	render() {
		const { open } = this.state,
			{ muscles } = this.context;

		return <>
			<Fab aria-label="Add"
			     color="secondary"
			     onClick={this.handleToggle}
			>
				<AddIcon fontSize="small"/>
			</Fab>
			<Dialog
				open={open}
				onClose={this.handleToggle}
				fullWidth
				maxWidth={"sm"} // default is 'md'
			>
				<DialogTitle>
					Create a new exercise
				</DialogTitle>

				<DialogContent>
					<DialogContentText>
						Please fill out the form below
					</DialogContentText>

					<Form muscles={muscles}
					      onSubmit={this.onSubmit}>
					</Form>
				</DialogContent>

			</Dialog>
		</>;
	}
}

export default CreateDialog;